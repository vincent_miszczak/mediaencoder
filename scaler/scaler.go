package main

import (
	"auchan/mediaencoder/backends"
	"auchan/mediaencoder/scaler/scalers"
	"fmt"
	"math"
	"os"
	"strconv"
	"time"
)

func sleep() {
	time.Sleep(5 * time.Second)
}

func main() {

	backendDriver := os.Getenv("DRIVER")
	if backendDriver == "" {
		panic("You must set DRIVER var")
	}

	url := os.Getenv("MARATHON")
	if url == "" {
		panic("You must set MARATHON var")
	}

	broker := os.Getenv("BROKER")
	if broker == "" {
		panic("You must set BROKER var")
	}

	queue := os.Getenv("QUEUE")
	if queue == "" {
		panic("You must set QUEUE var")
	}

	appid := os.Getenv("APPID")
	if appid == "" {
		panic("You must set APPID var")
	}

	perConverter, err := strconv.Atoi(os.Getenv("PERCONVERTER"))
	if err != nil {
		panic("You must set PERCONVERTER var")
	}

	m, err := strconv.Atoi(os.Getenv("MAX"))
	if err != nil {
		panic("You must set MAX workers value")
	}
	max := float64(m)

	scaler := scalers.NewMarathonScaler(url)
	backend := backends.GetBackend(backendDriver, broker, queue)

	for {

		if scaler.IsScaling(appid) {
			fmt.Printf("A deployment task is already running, skipping...\n")
			sleep()
			continue
		}

		qsize := backend.GetQueueSize()
		fmt.Printf("Queue size is %v \n", qsize)
		wantedUnbound := float64(qsize / perConverter)
		wanted := int(math.Min(max, math.Floor(wantedUnbound)+1))
		current := scaler.GetNoInstances(appid)
		if qsize == 0 && scaler.GetNoInstances(appid) > 1 {
			fmt.Println("Requesting scaling to 1")
			scaler.Scale(appid, 1)
		} else if current < wanted {
			fmt.Printf("Requesting scaling to %v\n", wanted)
			scaler.Scale(appid, wanted)
		} else {
			fmt.Printf("Queue size is %v and scaling is %v out of max %v, doing nothing\n", qsize, current, max)
		}
		sleep()
	}
}
