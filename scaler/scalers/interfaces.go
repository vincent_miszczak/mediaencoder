package scalers

type Scaler interface {
	Scale(id interface{}, instances int)
	GetNoInstances(id interface{}) int
	IsScaling(id interface{}) bool
}
