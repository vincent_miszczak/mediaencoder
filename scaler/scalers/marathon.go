package scalers

import (
	"log"

	marathon "github.com/gambol99/go-marathon"
)

type MarathonScaler struct {
	Client marathon.Marathon
}

func NewMarathonScaler(url string) MarathonScaler {
	config := marathon.NewDefaultConfig()
	config.URL = url
	client, err := marathon.NewClient(config)
	if err != nil {
		log.Fatalf("Failed to create a client for marathon, error: %s", err)
	}
	return MarathonScaler{Client: client}
}

func (m MarathonScaler) Scale(id interface{}, instances int) {
	if _, err := m.Client.ScaleApplicationInstances(id.(string), instances, true); err != nil {
		log.Fatalf("Failed to delete the application, error: %s", err)
	} else {
		log.Printf("Successfully scaled the application")
	}
}

func (m MarathonScaler) GetNoInstances(id interface{}) int {
	app, err := m.Client.Application(id.(string))
	if err != nil {
		panic("Could not get application")
	}
	return *app.Instances
}

func (m MarathonScaler) IsScaling(id interface{}) bool {
	app, err := m.Client.Application(id.(string))
	if err != nil {
		panic("Could not get application")
	}
	if len(app.Deployments) != 0 {
		return true
	}
	return false
}
