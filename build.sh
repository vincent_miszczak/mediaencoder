#!/bin/bash
set -e
go build -o apiserver/apiserver apiserver/apiserver.go
go build -o converter/converter converter/converter.go
go build -o scaler/scaler scaler/scaler.go
docker-compose build
