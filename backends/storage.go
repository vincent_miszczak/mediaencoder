package backends

type Storage interface {
	Push(name string, data []byte, metadata map[string]string)
}
