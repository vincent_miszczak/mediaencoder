package backends

import (
	"auchan/mediaencoder/model"
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

type Rmq struct {
	queue   amqp.Queue
	channel *amqp.Channel
	conn    *amqp.Connection
	writer  chan string
	reader  <-chan amqp.Delivery
}

func NewRmqWriter(address string, queue string) model.Backend {

	conn, err := amqp.Dial("amqp://" + address)
	failOnError(err, "Failed to connect to RabbitMQ")
	ch, err := conn.Channel()
	q, err := ch.QueueDeclare(
		"convert", // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	failOnError(err, "Failed to declare a queue")

	rmq := Rmq{
		queue:   q,
		channel: ch,
		conn:    conn,
		writer:  make(chan string, 1024)}
	rmq.doWrites()

	return rmq
}

func NewRmqReader(address string, queue string) model.Backend {

	conn, err := amqp.Dial("amqp://" + address)
	failOnError(err, "Failed to connect to RabbitMQ")
	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	err = ch.Qos(10, 0, false)
	if err != nil {
		panic(err)
	}
	q, err := ch.QueueDeclare(
		"convert", // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	failOnError(err, "Failed to declare a queue")

	rmq := Rmq{
		queue:   q,
		channel: ch,
		conn:    conn,
		writer:  make(chan string, 1024)}

	reader, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	rmq.reader = reader
	return rmq
}

func NewRmq(address string, queue string) model.Backend {

	conn, err := amqp.Dial("amqp://" + address)
	failOnError(err, "Failed to connect to RabbitMQ")
	ch, err := conn.Channel()
	q, err := ch.QueueDeclare(
		"convert", // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	failOnError(err, "Failed to declare a queue")

	rmq := Rmq{
		queue:   q,
		channel: ch,
		conn:    conn,
		writer:  make(chan string, 1024)}

	return rmq
}

func (r Rmq) Write(data string) {
	r.writer <- data
}

type RabbitMQDelivery struct {
	message  string
	delivery amqp.Delivery
}

func (d RabbitMQDelivery) GetMessage() string {
	return d.message
}

func (d RabbitMQDelivery) Ack() {
	d.delivery.Ack(false)
}

func (d RabbitMQDelivery) Nack(requeue bool) {
	d.delivery.Nack(false, requeue)
}

func (r Rmq) Read() model.Delivery {
	delivery := <-r.reader
	message := delivery.Body
	return RabbitMQDelivery{message: string(message), delivery: delivery}
}
func (r Rmq) doWrites() {
	go func() {
		for {
			data := <-r.writer
			ch := r.channel
			q := r.queue
			err := ch.Publish(
				"",     // exchange
				q.Name, // routing key
				false,  // mandatory
				false,  // immediate
				amqp.Publishing{
					ContentType: "application/json",
					Body:        []byte(data),
				})
			failOnError(err, "Failed to publish a message")
		}
	}()
}

func (r Rmq) GetQueueSize() int {
	queue, _ := r.channel.QueueInspect(r.queue.Name)
	return queue.Messages
}

func init() {
	RegisterBroker("RABBITMQ_WRITER", NewRmqWriter)
	RegisterBroker("RABBITMQ_READER", NewRmqReader)
	RegisterBroker("RABBITMQ_SCALER", NewRmq)
}
