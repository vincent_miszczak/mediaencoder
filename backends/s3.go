package backends

import (
	"io"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type S3Storage struct {
	uploader *s3manager.Uploader
	Bucket   string
	Key      string
	Region   string
}

func NewS3Storage(bucket, region string) S3Storage {
	// The session the S3 Uploader will use
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(region)}))

	// Create an uploader with the session and default options
	uploader := s3manager.NewUploader(sess)
	ret := S3Storage{uploader: uploader,
		Bucket: bucket,
		Region: region}
	return ret
}

//Implements Storage interface
func (s3 S3Storage) Push(key string, f io.Reader, metadata map[string]*string) (string, error) {
	uploader := s3.uploader
	// Upload the file to S3.
	uploadObject := s3manager.UploadInput{
		Bucket:      aws.String(s3.Bucket),
		Key:         aws.String(key),
		Body:        f,
		Metadata:    metadata,
		ContentType: aws.String("image/jpg")}

	_, err := uploader.Upload(&uploadObject)

	if err != nil {
		log.Print("failed to upload file, %v", err)
		return "", err
	} else {

	}
	return "https://s3." + s3.Region + ".amazonaws.com/" + s3.Bucket + key, nil
}
