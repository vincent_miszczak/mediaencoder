package backends

import (
	"auchan/mediaencoder/model"

	"github.com/Shopify/sarama"
	cluster "github.com/bsm/sarama-cluster"
)

type KafkaBackend struct {
	producer sarama.SyncProducer
	consumer *cluster.Consumer
	Topic    string
}

type KafkaDelivery struct {
	consumer        *cluster.Consumer
	consumerMessage *sarama.ConsumerMessage
	writer          KafkaBackend
}

func (d KafkaDelivery) Ack() {
	d.consumer.MarkOffset(d.consumerMessage, "ack")
	d.consumer.CommitOffsets()
}

func (d KafkaDelivery) GetMessage() string {
	return string(d.consumerMessage.Value)
}

func (d KafkaDelivery) Nack(requeue bool) {
	d.Ack()
	if requeue {
		d.writer.Write(string(d.consumerMessage.Value))
	}
}

func (k KafkaBackend) Write(msg string) {
	producerMessage := &sarama.ProducerMessage{
		Topic: k.Topic,
		Value: sarama.StringEncoder(msg)}

	_, _, err := k.producer.SendMessage(producerMessage)
	if err != nil {
		panic(err)
	}
}

func (k KafkaBackend) Read() model.Delivery {
	message := <-k.consumer.Messages()
	delivery := KafkaDelivery{
		consumer:        k.consumer,
		consumerMessage: message,
		writer:          k}
	//fmt.Println("DEBUG:message is " + string(message.Value))
	return delivery
}

func (k KafkaBackend) GetQueueSize() int {
	return len(k.consumer.Messages())
}

func NewKafkaReader(addr string, topic string) model.Backend {
	config := cluster.NewConfig()
	config.Consumer.Return.Errors = true
	config.Group.Return.Notifications = true
	consumer, err := cluster.NewConsumer([]string{addr}, "mediaencoder", []string{topic}, config)

	if err != nil {
		panic(err)
	}
	backend := KafkaBackend{consumer: consumer,
		Topic: topic}

	return backend
}

func NewKafkaWriter(addr string, topic string) model.Backend {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = -1
	config.Producer.Return.Successes = true
	config.Producer.Partitioner = sarama.NewRoundRobinPartitioner
	producer, err := sarama.NewSyncProducer([]string{addr}, config)
	if err != nil {
		panic(err)
	}
	backend := KafkaBackend{
		producer: producer,
		Topic:    topic}
	return backend
}

func init() {
	RegisterBroker("KAFKA_WRITER", NewKafkaWriter)
	RegisterBroker("KAFKA_READER", NewKafkaReader)
}
