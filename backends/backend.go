package backends

import "auchan/mediaencoder/model"

type brokerConstructor func(string, string) model.Backend

var brokers = make(map[string]brokerConstructor)

func RegisterBroker(name string, constructor brokerConstructor) {
	brokers[name] = constructor
}

func GetBackend(name string, address string, topic string) model.Backend {
	return brokers[name](address, topic)
}
