package main

import (
	"auchan/mediaencoder/apiserver/driver"
	"auchan/mediaencoder/apiserver/web"
	"auchan/mediaencoder/backends"
	"os"
)

func main() {
	backendDriver := os.Getenv("DRIVER")
	if backendDriver == "" {
		panic("You must set DRIVER var")
	}

	broker := os.Getenv("BROKER")
	if broker == "" {
		panic("You must set BROKER var")
	}

	queue := os.Getenv("QUEUE")
	if queue == "" {
		panic("You must set QUEUE var")
	}

	backend := backends.GetBackend(backendDriver, broker, queue)
	driver.SetBackend(backend)
	web.Start(web.GetRouter())
}
