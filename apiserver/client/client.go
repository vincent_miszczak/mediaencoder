package main

import (
	"auchan/mediaencoder/model"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"
)

func main() {
	sem := make(chan bool, 24)
	var wg sync.WaitGroup
	source := "http://www.jqueryscript.net/images/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg"
	for i := 0; i < 10000; i++ {
		q := model.Convert{
			AssetID: strconv.Itoa(i),
			Source:  source,
			Formats: []model.Format{model.Format{Width: 200}, model.Format{Width: 400}}}

		data, _ := json.Marshal(q)
		sdata := string(data)
		reader := strings.NewReader(sdata)
		sem <- true
		wg.Add(1)
		go func() {
			response, err := http.Post("http://apiserver-mediaencoder.prod01.dcos.aec.corp/api/v1/convert", "application/json", reader)
			defer func() {
				<-sem
				wg.Done()
			}()
			if err != nil {
				fmt.Printf("Got error %v\n", err)
				return
			} else {
				response.Body.Close()
			}
			if response.StatusCode != 200 {
				fmt.Printf("Did not get 200\n")
			}

		}()
	}
	wg.Wait()
}
