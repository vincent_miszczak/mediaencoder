package driver

import "auchan/mediaencoder/model"

import "encoding/json"
import "log"

var b model.Backend

func SetBackend(backend model.Backend) {
	b = backend
}

func Convert(req model.Convert) {
	data, err := json.Marshal(req)
	str := string(data)
	if err != nil {
		log.Fatal("?!")
	}
	b.Write(str)
}
