package web

import (
	"auchan/mediaencoder/apiserver/driver"
	"auchan/mediaencoder/model"
	"net/http"

	"encoding/json"

	"github.com/gorilla/mux"
)

func error(w http.ResponseWriter) {
	w.WriteHeader(400)
}

func convert(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var model model.Convert
	err := json.NewDecoder(r.Body).Decode(&model)
	if err != nil {
		error(w)
		return
	}
	driver.Convert(model)
}

//----------------------------------------------------------------

func GetRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/api/v1/convert", convert).Methods("POST")
	return r
}

func Start(router *mux.Router) {
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		panic(err)
	}
}
