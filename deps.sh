go get -u github.com/streadway/amqp
go get -u github.com/aws/aws-sdk-go/service/s3/s3manager
go get -u github.com/nfnt/resize
go get -u github.com/gorilla/mux
go get -u github.com/gambol99/go-marathon
go get -u github.com/golang/glog
