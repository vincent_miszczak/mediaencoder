package main

import (
	"auchan/mediaencoder/backends"
	"auchan/mediaencoder/model"
	"os/signal"
	"syscall"

	"fmt"
	"image/jpeg"
	"io"
	"net/http"
	"os"

	"image"

	"sync"

	"github.com/nfnt/resize"
)

type Conversion struct {
	delivery model.Delivery
	format   model.Format
	request  model.Convert
	image    *image.Image
}

type DeliveryResults struct {
	delivery  model.Delivery
	request   model.Convert
	noResults int
	results   chan bool
}

func checkResults(deliveryResults DeliveryResults) {
	for i := 0; i < deliveryResults.noResults; i++ {
		result := <-deliveryResults.results
		if result == false {
			deliveryResults.delivery.Nack(false)
			return
		}
	}
	deliveryResults.delivery.Ack()
}

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	backendDriver := os.Getenv("DRIVER")
	if backendDriver == "" {
		panic("You must set DRIVER var")
	}

	awsRegion := os.Getenv("AWS_REGION")
	if awsRegion == "" {
		panic("You must set AWS_REGION var")
	}

	broker := os.Getenv("BROKER")
	if broker == "" {
		panic("You must set BROKER var")
	}

	queue := os.Getenv("QUEUE")
	if queue == "" {
		panic("You must set QUEUE var")
	}

	bucket := os.Getenv("BUCKET")
	if bucket == "" {
		panic("You must set BUCKET var")
	}

	backend := backends.GetBackend(backendDriver, broker, queue)
	storage := backends.NewS3Storage(bucket, awsRegion)

	conversions := make(chan bool, 10)

	var wg sync.WaitGroup
	exiting := make(chan bool, 1)

	go func() {
		<-sigs
		exiting <- true
		wg.Wait()
		fmt.Println("Clean exit")
		os.Exit(0)
	}()

	for {
		select {
		case <-exiting:
			exiting <- true
			exiting <- true
		default:
		}
		delivery := backend.Read()

		var convertRequest model.Convert
		model.FromJson(delivery.GetMessage(), &convertRequest)

		src, err := http.Get(convertRequest.Source)
		if err != nil {
			src.Body.Close()
			fmt.Println("ERROR while fetching source image from " + convertRequest.Source)
			delivery.Nack(true)
			continue
		}

		img, err := jpeg.Decode(src.Body)
		if err != nil {
			src.Body.Close()
			fmt.Println("ERROR while decoding source image")
			delivery.Nack(true)
			continue
		}
		src.Body.Close()

		deliveryResults := DeliveryResults{
			delivery:  delivery,
			noResults: len(convertRequest.Formats),
			results:   make(chan bool, len(convertRequest.Formats)),
			request:   convertRequest}

		go checkResults(deliveryResults)

		for _, format := range convertRequest.Formats {
			conversion := Conversion{delivery: delivery, format: format, request: convertRequest, image: &img}
			conversions <- true
			wg.Add(1)
			go convert(conversions, conversion, deliveryResults, storage, &wg)
		}
	}
}

func convert(conversions chan bool, conversion Conversion, deliveryResults DeliveryResults, storage model.Storage, wg *sync.WaitGroup) {
	img := *conversion.image
	m := resize.Resize(conversion.format.Width, conversion.format.Height, img, resize.Lanczos3)
	pr, pw := io.Pipe()
	go func() {
		defer func() {
			<-conversions
			wg.Done()
		}()

		metadata := make(map[string]*string)
		w := fmt.Sprint(conversion.format.Width)
		h := fmt.Sprint(conversion.format.Height)

		metadata["assetID"] = &conversion.request.AssetID
		metadata["width"] = &w
		metadata["height"] = &h

		url, err := storage.Push("/media/"+conversion.request.AssetID+"/"+conversion.request.AssetID+"-"+w+"-"+h+".jpg", pr, metadata)
		pr.Close()
		if err != nil {
			deliveryResults.results <- false
			return
		}
		convertCallback := model.Convert(conversion.request)
		convertCallback.Formats = []model.Format{conversion.format}
		convertCallback.Source = url
		fmt.Println(convertCallback)
		deliveryResults.results <- true
	}()
	jpeg.Encode(pw, m, nil)
	pw.Close()
	fmt.Println("Encoding done")
}
