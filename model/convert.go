package model

type Format struct {
	Width  uint `json:"width"`
	Height uint `json:"height"`
}
type Convert struct {
	AssetID string   `json:"assetID"`
	Source  string   `json:"source"`
	Formats []Format `json:"formats"`
}
