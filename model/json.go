package model

import "encoding/json"

func FromJson(s string, i interface{}) {
	json.Unmarshal([]byte(s), &i)
}

func ToJson(c interface{}) string {
	data, _ := json.Marshal(c)
	return string(data)
}
