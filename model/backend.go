package model

import "io"

type Delivery interface {
	Ack()
	Nack(requeue bool)
	GetMessage() string
}

type Backend interface {
	Write(data string)
	Read() Delivery
	GetQueueSize() int
}

type Storage interface {
	Push(key string, f io.Reader, metadata map[string]*string) (string, error)
}
